# Aparato CiFi -- Libros

Este repositorio contiene _todos_ los libros y material base
para para su edición.

## ¡Descarga!

Todos los libros cuentan con los siguientes formatos:

* PDF facsimilar.
* PDF limpio.
* EPUB.
* MOBI.
* HTML.

¿Qué esperas para descargarlos? Solo ve a la carpeta 
[`./out`](https://gitlab.com/NikaZhenya/cifi-aa/tree/master/out).

## Nota sobre libros electrónicos

Las ediciones de los libros electrónicos se prestan a algunas pequeñas
diferencias con respecto a sus pares impresos aquí digitalizados:

* Se corrigieron los errores del OCR fruto de la digitalización, aunque
  bien puede haberse colado alguna errata que no está en el original.
* Se modernizó la ortografía según los criterios ortográficos de la RAE
  establecidos a partir de 2010. En su mayoría fue la eliminación de tildes
  en pronombres demostrativos, la palabra «solo» y monosílabos. O adición
  de tildes en letras versales.

Pese a estas diferencias, _en ningún caso_ se realizó corrección de estilo.
La redacción corresponde según a la versión impresa digitalizada.

## Árbol de directorios

```
.
├── build
│   └── src
│       ├── img
│       └── xhtml
├── out
├── src
├── .gitignore
├── LICENSE-CODE
└── README.md

```

* `build`. Contiene los _scripts_ para la producción de libros.
* `build/src`. Contiene los recursos para la producción.
* `build/src/img`. Contiene las imágenes que se añaden a todos los libros.
* `build/src/xhtml`. Contiene las plantillas de las preliminares de los libros.
* `out`. Contiene los archivos de salida: EPUB, HTML, MOBI, PDF, PDF facsimilar y portada.
* `src`. Contiene los archivos de entrada: MD del libro, MD de notas o imágenes.
* `.gitignore`. Archivo que ignora cierto tipo de ficheros.
* `LICENSE-CODE`. Licencia del código.
* `README.md`. Este texto.

## _Scripts_ para su producción

### Autocorrección de _ebooks_

A partir de los archivos de Markdown es posible autocorregir los libros
electróncios. Ejecuta `./build/autocorrect -h` para más información.

### Desarrollo automatizado de _ebooks_

A partir de los archivos de Markdown es posible desarrollar los libros
electrónicos. Ejecuta `./build/develop-ebooks -h` para más información.

## Créditos

Este repositorio completamente automatizado fue desarrollado por
Programando LIBREros.

* Contacto: hi[at]perrotuerto.blog

## Licencia

El código de este proyecto está bajo 
[licencia GPLv3+](https://gnu.org/licenses/gpl.html).

Los libros electrónicos están bajo
[Licencia Editorial Abierta y Libre (LEAL)](http://leal.perrotuerto.blog)

Los PDF son de acceso abierto y sin licencia.
