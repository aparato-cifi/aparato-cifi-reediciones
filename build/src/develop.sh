pc-automata -f ../@path/archivos-madre/md/todo.md \
            -c ../portada.jpg \
            -s ../build/src/styles.css \
            -i ../build/src/img \
            -x ../xhtml \
            @images? \
            @notes? \
            --section \
            --compress \
            --no-legacy \
            --no-analytics \
            --no-ace \
            --overwrite

pc-pandog -i ../@path/archivos-madre/md/todo.md -o ./todo.html
