<section epub:type="chapter" role="doc-chapter">

# Un viaje celeste

_El hombre es el ciudadano del cielo._ \
++Flammarion++ {.epigrafe}

Mis ojos no podían desprenderse de esta línea, cuyos
caracteres brillaban con mágica luz. Recordaba que
Sócrates dijo: «El hombre es el ciudadano del mundo».
Pero como esta raquítica esfera es importante
para calmar nuestras aspiraciones, ilustre astrónomo
ha procurado con frase sublime nuestra legítima ambición.
Es cierto que el cielo no basta para llenar el
alma; pero el infinito es el velo con que se cubre
Dios, y tarde o temprano el Supremo Ideal habrá satisfecho
el anhelo de nuestro espíritu.

Mi absorción era completa; pero a poco iba olvidándolo
todo; mis ojos fueron perdiendo la percepción;
caí lentamente en una especie de sonambulismo
espontáneo. Mis sentidos se entorpecieron, pero
mi inteligencia no estaba embotada; con los ojos del
alma lo veía todo, comprendía lo que me estaba pasando;
pero aquel éxtasis, compuesto de no sé qué
voluptuosidades extrañas, era tan dulce, había en él
una mezcla tan indefinible de ideas, de delirios, de
fruiciones desconocidas, que en lugar de resistirme,
me dejaba arrastrar por aquella languidez llena de encanto
y también de vida. ¡Oh, yo quisiera estar siempre
así!

Mi alma se fue desprendiendo de mi cuerpo como
si fuese un vapor, un éter, un perfume; la veía, es
decir, me veía a mí mismo, como si estuviese formado
de gasa o de crespón aparente, y sin embargo
real, pero con todas aquellas ondulaciones, ligerezas
y flexibilidades que tiene lo intangible.

Aquello era maravilloso; la sorpresa que me
causaba mi nuevo estado no me dejaba ya lugar a la
reflexión; mi pobre cuerpo yacía exánime, sin movimiento,
en una postración absoluta. Comencé a creer
que había muerto, pero de una manera tan dulce, tan
bella, que no me arrepentía; antes bien estaba resuelto
a principiar nuevamente. Algunos momentos después
me hallaba convencido hasta la opresión de mi
nuevo estado, y con una gratitud inmensa al Creador
que había cortado con tanta dulzura el hilo de mi
triste vida.

¡Cosa rara!, mi vista adquirió una penetración y
un alcance admirable; las paredes de la habitación
las veía transparentes como si fuesen de cristal; la
materia toda diáfana, límpida, incolora y clara como
el agua pura; veía infinidad de animalículos pequeñísimos
habitándolo todo; los átomos flotantes
del aire estaban poblados de seres; las moléculas
más imperceptibles palpitaban bajo el soplo omnipotente
de la vida y del amor… Mis demás sentidos
se habían desarrollado en la misma proporción, y
me sentía feliz, os lo aseguro; intensamente feliz.

Al verme dotado con tan bellas facultades, mi
vacilación fue muy corta; levanté la mirada… y caí
anonadado al contemplar la magnificencia de los
cielos.

Oré un instante, y con la rapidez del pensamiento,
me lancé a vagar por el bellísimo jardín de la creación.
En mi estado normal veo a las estrellas, melancólicas
pupilas, fijas sobre la Tierra; rubíes, brillantes,
topacios, esmeraldas y amatistas, incrustadas en
un espléndido zafiro, pero entonces… ¡Oh!… entonces
voy a referiros con más calma lo que vi.

Es preciso que ordene algo mis ideas.

Comenzaré, pues, por deciros que me bastaba
pensar para que siguiese al pensamiento la más rápida
ejecución, y por lo mismo, la idea que había tenido
de ascender por los espacios me alejó de la Tierra
a una distancia inmensa.

A lo lejos veía una esfera colosal ---un millón quinientas
mil veces mayor que la Tierra---, incandescente
como el ojo sangriento de una fiera, roja como el
fuego, volaba con velocidad, arrastrando en aquella
carrera una multitud de esferas, entre las cuales había
algunas algo aplanadas por dos puntos, pero todas
de mucho menores dimensiones, pues si hubieran
podido reunirse no igualarían con su volumen al
hermosísimo disco de fuego; a pesar de que se encontraban
algo lejanas, las percibía con una claridad
extraordinaria, capaz de permitirme examinar hasta
sus menores detalles.

Figuraos mi asombro: aquella antorcha encendida
en medio de los cielos era nuestro Sol, y sus
acompañantes, su familia de planetas.

Pero no era todo, no: lo que me dejaba mudo,
absorto, enajenado, era que todas aquellas masas
enormes eran ¡mundos! más o menos semejantes al
nuestro, pero todos ellos, sin excepción, mundos
habitados.

Sí, sí, yo veía las manchas blancas de las nieves
polares, las nubes cruzando sus atmósferas, las unas
densas, cargadas de brumas, las otras purísimas y tenues,
los mares brillaban como líquida plata, y los
continentes parecían inmensas aves que se recostaban
cansadas de volar.

Allí hay seres, me decía yo, seres humanos, habitantes,
hombres tal vez, y ángeles como los que
habitan la Tierra con nombres de mujeres, porque si
no fuera así, esos mundos serían horribles; allí estarán
mis hermanas, mis padres, mi familia…

¡Oh Dios mío, cómo a la vista de esos mundos se
despliega tu soberana omnipotencia!

Entonces busqué a Júpiter, que de los planetas
de nuestro sistema es el mayor y el más bello; la Tierra
la veía como la 1/126 parte del brillante astro, que
me deslumbró por su hermosura; esto en cuanto a
superficie.

Sus montañas tienen una inclinación muy suave,
sus llanuras son perfectamente planas, los mares
tranquilos; nada de nieve; la eterna primavera bordando
sus campos, flores divinas embriagando con
sus deliciosos aromas a esos felices habitantes, aves
de pintados colores cruzando en todas direcciones,
y cuatro magníficas lunas que deben producir en
sus serenas y apacibles noches unos juegos de luz
admirables.

Multitud de ciudades diseminadas sobre su superficie,
pero por más que lo procuré no puede distinguir
los habitantes; tal vez serán de una belleza
deslumbradora, que después me hubieran hecho despreciar
los de la Tierra, y por eso la Providencia me
evitó el verlos. Júpiter es un mundo en el cual el dolor
no es conocido, es un verdadero Edén.

Mercurio y Venus no llamaban mi atención, la
Tierra me daba cólera por orgullosa, Marte tiene tantos
cataclismos y cambios que tampoco me agradaba,
los asteroides me parecían muy pequeños, olvidé
a Saturno, a Urano, y después de mi hermoso Júpiter,
mi futura patria, pensé en Neptuno, que según
la mitología representa al dios de las aguas.

Aquello fue un salto peligroso; en menos de un
segundo atravesé centenares de millones de leguas y
me encontré a una distancia regular del astro que por
hoy limita nuestro sistema. Entonces no comprendí
muy bien lo que me pasaba: el Sol lo veía del tamaño
de una lenteja, Saturno enorme, como de un volumen
de setecientas treinta y cuatro veces mayor
que la Tierra, y yo me hallaba en una penumbra indefinible.

La naturaleza, como la obra de Dios, es admirable;
apenas pude distinguir que aquel mundo, como
los otros, estaba habitado; pero previendo la lejanía
del Sol, los seres que allí viven tienen la facultad de
desprender luz, están rodeados de una aureola luminosa,
tan bella, que fascinado no podía apartar de
ellos mi vista embelesada con su contemplación.

Me fue imposible fijarme en más detalles, porque
en un momento me sentí arrastrado por una
fuerza extraña; observé lo que era: la cauda de un
cometa me envolvía, me encontraba en una línea de
atracción del astro errante, que sacudía su magnífica
cabellera en la inmensidad.

El vehículo celeste era cómodo y bello; me dejé
llevar sin oponer resistencia. La velocidad de mi
tren expreso iba aumentando cada vez más; cruzábamos
los abismos dejando a nuestros pies infinitas
miríadas de mundos.

Repentinamente observé que una estrella doble,
púrpura y oro, crecía a mi vista de una manera espantosa;
en algunos segundos adquirió proporciones
gigantescas, como de unas diez veces más que nuestro
Sol; sentí una atmósfera de fuego, y abandonando
mi solitario compañero me lancé huyendo en dirección
opuesta.

Os he dicho ya que volaba por los cielos con la
velocidad del pensamiento; los soles de colores se
multiplicaban a mi vista, ya rojos o violados, amarillos
o verdes, blancos o azules, y alrededor de cada
uno de ellos flotaban infinidad de mundos en los
cuales palpitaba también la vida y el amor.

Yo seguía corriendo, volando con una rapidez
vertiginosa, atravesaba las inmensas llanuras celestes
bordadas de flores, me sentía arrastrado por lo
invisible, y trémulo y palpitante, yo balbuceaba una
oración.

Aquello no terminaba nunca, nunca… La alfombra
de soles que Dios tiene a sus pies se prolongaba
hasta lo infinito… se pasaron instantes o siglos, no lo
sé; yo seguía con mayor velocidad que la luz, que la
Chispa eléctrica, que el pensamiento, y aquella magnífica
contemplación seguía también… soles inmensos
de todos colores, mundos colosos girando a su
derredor, y todo… todo lleno de vida, de seres, de almas
que bendecían a Dios. Los soles cantando con
voz luminosa y los mundos elevando sus himnos
formaban el concierto sublime, grandioso, divino de
la armonía universal.

Atravesaba los desiertos del espacio cruzando
de una nebulosa a otra; la extensión seguía; atravesaba
multitud de vías lácteas en todas direcciones, y
volaba… seguía… y la inmensidad seguía también.

Estaba jadeante, rendido, abrumado; oraba con
fervor y me sentía arrastrar por una fuerza irresistible:
los abismos, los espacios, las nebulosas, los soles
y los mundos se sucedían sin interrupción, se
mezclaban, se agitaban en turbiones armónicos sobre
mi frente humillada, abatida ante tanta magnificencia,
ante tan deslumbrante esplendidez. Yo estaba
ciego, loco, casi no existía ya; pequeño átomo
perdido en aquella inmensidad, apenas me atrevía a
murmurar conmovido, temblando, admirado ante la
manifestación divina de la Omnipotente Causa
Creadora, ¡Dios mío! ¡Dios mío!

De pronto mi carrera cesó… Dios escuchaba al
átomo.

Tardé algún tiempo en reponerme; perdido en la
extensión sideral, busqué en vano la Tierra; nada, no
se veía; quise encontrar nuestro Sol, pero imposible;
tampoco lo veía.

Apenas allá a lo lejos, a una distancia incalculable,
perdida en los abismos sin límites de la eternidad,
pude ver nuestra Vía Láctea, que parecía una
pequeña cinta de plata formando un círculo de dimensiones
como el de una oblea, que volaba con
una velocidad inapreciable en la profundidad divina
de las regiones infinitas. Ligero y veloz me lancé hacia
ella; pronto llegué, sin saber cómo; pero entre
sus setenta millones de soles no podía encontrar el
nuestro. Pensé entonces que con la velocidad de la
luz tardaría quince mil años en dar una vuelta a
nuestra pequeña Vía Láctea, y abrumado por aquel
cálculo, sin poder comprenderlo, oprimido por semejante
idea, me detuve lleno de terror. ¿Qué hacer?
¿Cómo hallar la miserable chispa que llamamos
Sol? ¿Cómo encontrar la Tierra, átomo mezquino,
molécula despreciable, excrecencia diminuta de
aquel sol que no podía hallar por su pequeñez? ¡Oh!
Entonces mi alma, desfallecida, ansiosa, anhelante,
se dirigió a Dios.

¡Oh Tú, espléndido sol de los soles, Supremo
Ideal de las almas, Espíritu de Luz y de Vida, Amor
Infinito de la Inmensidad de la Creación, del Universo!…
¡Oh, Tú, mi Dios, vuélveme a mi átomo y perdona
mi loco orgullo, vuélveme a la Tierra, Dios
mío, porque allí está lo que yo amo!

Mi carrera comenzó de nuevo terrible, frenética,
espantosa; sentía vértigo, un ansia atroz, algo como
el frío de la muerte; corría, volaba y… en ese momento
Manuel de Olaguíbel me sacudió fuertemente por
el brazo; yo me encontraba sentado en mi escritorio,
con el pelo algo quemado, las manos convulsas, multitud
de papeles en desorden, y escritas las anteriores
líneas.

---¿Qué tienes? ---me dijo mi amigo.

---Nada ---le contesté algo turbado todavía---;
es que el cielo…

---Sí, el cielo ---me dijo riéndose---… hace largo
rato que te observo; tenías un verdadero delirio, gesticulabas,
escribías; yo iba leyendo, pero me pareció
prudente suspender esa carrera fantástica, por temor
de que la terminases en un hospital de dementes.

---El cielo, el cielo ---repetía yo maquinalmente.

---Sí ---continuó---; el cielo es lo más bello que
hay, supuesto que es lo que nos manifiesta y enseña
la Omnipotencia Suprema de Dios; tú en esas líneas
dices poco de Él; pero, sin embargo, todas son
verdades científicas, axiomáticas, irreductibles,
que forman el patrimonio que el siglo impío deja al
porvenir.

Salimos; el viento fresco de la noche calmó mi
exaltación; pero por más que lo procuro, no puedo
dejar de pensar que el Universo es la patria de la humanidad
y el hombre el ciudadano del cielo.

</section>
