Ver Anexo II.

La primera referencia que encontré sobre este curioso cuento
está en el capítulo «Fantasía y realidad» del libro _La literatura
perseguida en la crisis de la Colonia_, de Pablo González Casanova
(México: SEP, 1986). Con algunos desajustes paleográficos está
publicado en [ciencia-ficcion.com.mx](http://www.ciencia-ficcion.com.mx).

_Sobre la ciencia ficción_, Buenos Aires, Editorial Sudamericana,
1999, pp. 18 y 21.

_Ciencia ficción_. _Las 100 mejores novelas_, Barcelona, Minotauro,
1995, pp. 11-15. Pringle afirma que existen otras dos formas de
ficción fantástica contemporánea: el relato de horror sobrenatural
y la fantasía heroica. En la línea de Tzvetan Todorov, considera
que la primera, en la que quedarían incluidas narraciones como
las de Bram Stoker, Lovecraft o Stephen King, se caracteriza
por la irrupción de fuerzas sobrenaturales o terroríficas en
lo cotidiano, inexplicables desde una cosmovisión científica
y racional, mientras que en la segunda, uno de cuyos prototipos
sería _El Señor de los Anillos_ de J.R.R. Tolkien, predomina la
magia y los mundos imaginarios de tierras fantásticas o paradisíacas.

+++VV.AA.+++, _Las 100 mejores novelas de ciencia ficción del
siglo +++XX+++_, Madrid, La Factoría de Ideas, 2002, p. 9.

_Metamorfosis de la ciencia ficción_, México, Fondo de Cultura
Económica, 1984, _pássim_.

«Historias que tratan de un mundo cambiado que aún no se ha hecho
realidad», John Clute. «Una narrativa que toma en cuenta el saber
científico para la elaboración de propuestas imaginativas que
pregonen los problemas inherentes a la condición humana cuando
se ve enfrentada a cambios y rupturas en todos los órdenes de
la existencia», Gabriel Trujillo Muñoz. «Un relato del futuro
puesto en pasado», Daniel Link. «En el fondo, la literatura de
ciencia ficción es la rama apocalíptica de la sociología y de
algunos sistemas políticos», Roberto Pliego. Las dos primeras
en _Las 100 mejores novelas de ciencia ficción del siglo +++XX+++_,
p. 10. La tercera en Daniel Link (comp.), _Escalera al cielo.
Utopía y ciencia ficción_, Buenos Aires, La Marca, 1994, p. 7.
La cuarta en Roberto Pliego, _101 preguntas para ser culto_, México,
Grijalbo, 2007, p. 152.

Tomamos la cita de Asimov, p. 18.

La fascinación por el relato de Shelley llevaría a Aldiss a tributarle
un homenaje por medio de su recreación novelesca, no del todo
lograda, _Frankenstein desencadenado_ (1973).

Un crítico tan inteligente como Mario Vargas Llosa ha aseverado
que la ciencia ficción, igual que la literatura erótica y la
novela policial, al encarnar un artificio intelectual desprendido
de su contexto y con el que el lector difícilmente puede asociar
su propia vivencia, está condenada a ser menor. Un conocedor
de la «sf» objetaría sin duda esta postura alegando que, de hecho,
ocurre justamente lo contrario, que su éxito radica en la plena
identificación con las situaciones y problemáticas descritas,
por más hipotéticas o no verificables, desde un punto de vista
empírico, que sean. Véase _La verdad de las mentiras_, Madrid,
Punto de Lectura, 2005, p. 254. Llama la atención que en esta
recopilación de ensayos sobre la novelística del siglo +++XX+++,
algunos de ellos espléndidos, Vargas Llosa haya optado por comentar
_La granja de los animales_ de George Orwell, un apólogo tal vez
más acorde con sus preferencias literarias, en lugar de _1984_
(1949), hito de la novela contemporánea universal, fuera y dentro
de la ciencia ficción.

Como más tarde, ya en la década de los 60 del +++XX+++, se empezará
a hablar de «ficción especulativa» (identificada en inglés con
las mismas siglas «sf», _speculative fiction_) para distinguir
una narrativa preocupada por el estilo, las técnicas experimentales
y los aspectos humanos de la trama, de otra menos flexible, enfocada
sobre todo en la exactitud y los pormenores de los asuntos científicos
planteados.

_Historia cómica de los estados e imperios de la Luna_, en Carlos
García Gual, _Viajes a la Luna. De la fantasía a la ciencia ficción_,
Madrid, Biblioteca ELR Ediciones, 2005, p. 130. Todas las citas
proceden de esta edición, que García Gual, a su vez, ha tomado
de la edición de Espasa-Calpe de 1924. La traducción al castellano
es de J. Chabás y Martí. Los aldeanos sin ropa eran iroqueses,
pueblo amerindio que se asentaba en las inmediaciones del lago
Ontario.

Esta primera discusión acerca de las concepciones científicas
de la época instaura la tónica del texto, cuyo hilo narrativo
es interrumpido con frecuencia para dar paso a polémicas científicas,
en ocasiones de lo más alambicadas, sobre temas tan diversos
como la estructura de los átomos y las leyes de la materia, los
sentidos y su relación con los objetos ---sometidos a las leyes
de «simpatía»---, la refracción de la luz, la influencia de los
determinantes biológicos en el comportamiento humano y en la
organización y funcionamiento de los estados, el origen del universo,
los medios de propagación del sonido, etc.

_Historia cómica_…, pp. 139 y 140.

Es curiosa esta imagen pre-newtoniana. Aunque Isaac Newton fue
hombre del mismo siglo que Bergerac, sus _Principios matemáticos
de filosofía natural_ saldrían a la luz tres décadas más tarde
que _Historia cómica_…

A Bergerac le importa poco la incoherencia descriptiva de que
coexistan en la misma geografía una pradera que no opone obstáculos
a los ojos y un bosque.

_Historia cómica_…, p. 148.

Entre ellos, al propio Jerónimo Cardano, a Cornelio Agripa, a
César y a Tommaso de Campanella, a quien el demonio de Sócrates
recomienda acomodar hipócritamente sus pensamientos a los de
sus perseguidores de la Inquisición. Aquí hay un evidente guiño
literario al lector, pues el solar alude en su relato a _La ciudad
del sol_ (1602), la célebre utopía renacentista de Campanella,
en la cual se describe una ciudad en cuyo centro se levanta un
templo circular dedicado al rey astro y regido por un sacerdote
supremo denominado, precisamente, sol.

El desconocido es el mismo demonio, bajo otra de sus apariencias,
cicerone rescatador en los diversos aprietos por los que Bergerac
pasa en la Luna. En este vínculo entre preceptor y protegido
son constantes las alusiones a conductas sexualmente equívocas,
pues el solar no sólo lame y muerde al pasivo terráqueo, sino
con frecuencia lo acaricia, masajea, estira, etcétera, demostraciones
de afecto que trascienden el trato entre amo y mascota, el cual
también es sugerido con ironía.

_Historia cómica…_, p. 163.

_Ibidem_, p. 174.

_Ibidem_, p. 183.

_Ibidem_, p. 210. Salvo esta alusión, a lo largo del texto no hay
indicios de que los lunares estuvieran interesados, o al menos
al tanto de, los asuntos concernientes a la espiritualidad humana.

_Ibidem_, p. 223.

_Sizigias y cuadraturas_…, v 83. Todas las citas proceden de la
versión que figura en este volumen, paleografiada por Carolina
Depetris a partir del original que obra en el Archivo General
de la Nación en Ciudad de México.

Esta fecha lunar que, por lo menos nominalmente, no coincide
con la de la Tierra ---la carta del atisbador es del 9 del mes
de epifi del año de Nabonasar 2510---, da pie a una digresión
narrativa por medio de la cual el secretario, sin preocuparse
por aclarar cómo habría podido tener acceso a ese material, menciona
el libro II de _Las metamorfosis_ de Ovidio, donde se recoge el
mito griego del hijo de Febo, para informar que sólo unos anctítonas
consiguieron salvarse de las llamas ocultándose en unas cavernas.
_Sizigias y cuadraturas_…, v 84. El argumento de Faetón queda espléndidamente
sintetizado en el epígrafe que las náyades grabaron en su tumba:
«Aquí yace, auriga del carro paterno, Faetón; como no puedo dominarlo,
pereció por su audacia sin límites». _Las metamorfosis_, México,
Porrúa, 1998, p. 23.

De lo que se infiere que sí lo han visto en espíritu. El cuento
de Rivas hace referencia a problemas teológicos concretos y en
él hay saltos cualitativos de lo sensorial a lo extrasensorial,
de lo lógico a lo irracional, de lo mundano a la ultramundano.
Características que, a simple vista, habilitarían conceptuarlo
como literatura fantástica en estricto sentido.

_Sizigias y cuadraturas_…, v 87.

_Ibidem_, r 88.

_Ibidem_, v 89.

Con esta pulla se venga Bergerac, muy literariamente, de quienes
se mofaban de él por su prominente órgano olfativo.

El término no aparece en el _+++DA+++_. En _+++DRAE+++_: «_Astr_.
Conjunción u oposición de la Luna con el Sol».

_+++DA+++_: «Cuadratura. En la Astronomía se llama el efecto
cuadrado de la Luna con el Sol. Cuando es el que se sigue a la
conjunción, se llama cuadratura primera o cuarto creciente, y
el que se sigue a la oposición, cuadratura segunda o cuarto menguante».
_+++DRAE+++_: «_Astr_. Situación relativa de dos cuerpos celestes,
que en longitud o en ascensión recta distan entre sí respectivamente
uno o tres cuartos de círculo».

No existe el término en _+++DA+++_ ni en _+++DRAE+++_.

En el calendario egipcio, tercer mes de la estación de la cosecha
(Shemu).

Nabonasar fue rey de Babilonia en el siglo +++VI+++ a.C. Aquí
el término hace alusión a la era de Nabonasar, usada por los
astrónomos caldeos y sobre todo por Hiparco y Tolomeo. La era
de Nabonasar comienza el 26 de febrero del 747 a.C. El vocablo
no aparece en los diccionarios _+++DA+++_ y _+++DRAE+++_.

_+++DA+++_: «adj. que se aplica a las cartas o escritos con que
se dedica alguna obra o en que se nombra e instituye alguno en
heredero o se le confiere algún empleo». La semántica del término
no varía en _+++DRAE+++_.

_+++DA+++_: «El primer día de la Luna». La semántica del término
no varía en _+++DRAE+++_.

_+++DA+++_: «Cálculo o cómputo. Es voz griega de que usan comúnmente
los astrónomos en los cálculos de los eclipses». _+++DRAE+++_:
«_Astr_. Cálculo».

_+++DA+++_: «Época de los árabes con que cuentan sus años, desde
que el falso Profeta Mahoma se huyó de Meca, después de introducir
su falsa doctrina». _+++DRAE+++_: «Era de los musulmanes, que
se cuenta desde el año 622, en que huyó Mahoma de la Meca a Medina,
y que se compone de años lunares de 354 días, intercalando 11
de 355 en cada período de 30».

Muharram es el primer mes del año islámico y en él se conmemora
la Hégira.

Sebastián I de Portugal (1554-1578) acometió una cruzada contra
el poder turco expandido por el norte de África. Desoyendo los
consejos de su tío Felipe II de España, marchó hacia Marruecos
y su ejército fue aplastado por los turcos en Alcazarquivir.
Sebastián cargó contra las líneas enemigas seguido de unos pocos
hombres y nunca más se lo volvió a ver ni se encontró su cadáver.

Tisrí o Tishrei es el primer mes del calendario hebreo moderno,
y el séptimo en el orden de meses que aparece en la Biblia y
que comienza con la conmemoración de la salida de los hebreos
de Egipto. No es, sin embargo, mencionado en la Biblia, sino
en el Talmud. El nombre fue adoptado de los meses de la antigua
Babilonia, cuando los judíos estuvieron allí desterrados entre
586 a.C. y 536 a.C. después de haber sido llevados al exilio
por Nabucodonosor II. El vocablo no aparece en _+++DA+++_ ni
en _+++DRAE+++_.

«Por el medio irás más seguro».

Faetón o Faetonte era, en la mitología griega, hijo de Helios.
Dice la historia mítica que los amigos de Faetón se negaban a
creer que era hijo del Sol, de modo que éste acudió a su padre
quien juró, por las aguas del río Estigia, concederle lo que
pidiera. Faetón entonces solicitó a su padre conducir su carro
(el Sol) un día. Helios intentó disuadirle, pero Faetón finalmente
consiguió lo que quería. Al mando del carro fue presa del pánico
y perdió el dominio de los caballos blancos que tiraban de él.
Primero subió demasiado y enfrió la tierra. Luego descendió y
quemó mucha vegetación convirtiendo la mayor parte de África
en un desierto y quemando la piel de los etíopes hasta volverla
negra. Zeus tuvo que intervenir y derribó el carro desbocado
con un rayo. Faetón cayó y se ahogó en las aguas del río Erídano.

«El palacio del sol era».

Jean-Baptiste Du Halde (1674-1743) fue un historiador jesuita,
francés, que concentró su interés en el estudio de la historia,
la cultura y la sociedad de China.

Términos sin referencia conocida, de supuesta etimología griega.

_+++DA+++_: «Bailiaje. Especie de encomienda en el orden de caballería
de San Juan, comúnmente hoy llamada de Malta, que obtienen por
su antigüedad los Caballeros profesos y también por gracia particular
del Gran Maestre de la Religión». _+++DRAE+++_ mantiene la definición
del término.

No aparece el término el _+++DA+++_. En _+++DRAE+++_: «Estrecho
de mar».

_+++DA+++_: «El zumo que se saca de las pencas de la hierba llamada
sábila. Viene de la voz árabe cebar, mudada la _e_ en _i_, y añadiéndole
la partícula a se dijo acíbar». _+++DRAE+++_: «áloe, planta.
2. áloe, jugo de esta planta».

_+++DA+++_: «Dicótomo, ma. adj. que sólo tiene uso en la astronomía
para diferenciar la Luna, Venus y Mercurio perfectamente dimidiados
o en la dicotomía de las otras fases o aspectos». En _+++DRAE+++_
el vocablo deja de hacer referencia a la astronomía: «adj. Que
se divide en dos».

_+++DA+++_: «Paralaje o paralaxis. Term. de astrónom. Es la diferencia
del lugar verdadero de un astro, considerando mirarse del centro
de la tierra, al lugar aparente mirado de la superficie de ella».
_+++DRAE+++_: «_Astron_. Diferencia entre las posiciones aparentes
que en la bóveda celeste tiene un astro, según el punto desde
donde se supone observado».

_+++DA+++_: «Equinoccial. adj. de un term. Lo perteneciente al
equinoccio». El término también es aplicado a la línea equinoccial:
«La circunferencia del círculo máximo, que divide el globo terráqueo
en dos partes iguales, que son los hemisferios boreal y austral.
Esta corresponde al ecuador, que se considera en la esfera celeste:
y como en llegando el sol a él se celebran los equinoccios, le
llaman también equinoccial, aunque lo más común es aplicar este
término al de la tierra». _+++DRAE+++_ reitera la primera definición
del término y sólo consigna el uso de «línea equinoccial».

Este es el nombre dado a una supuesta isla que aparece en el
mapa del cosmógrafo Antonio Zeno en el siglo +++XIV+++ y que
estaba ubicada cerca de la Península del Labrador. La isla, no
obstante, nunca fue encontrada, de modo que, desde el siglo +++XVI+++
Estotilandia ha pasado a ser una tierra imaginaria.

El vocablo «turbillón» es una transcripción fónica del término
en francés _tourbillon_, esto es, «torbellino». Descartes utilizó
el término para desarrollar su _théorie des tourbillons_ que explica
la órbita celeste. Para él, el sistema solar es un torbellino
que arrastra los planetas. Cada planeta es el centro de un nuevo
torbellino que retiene en su proximidad la materia que lo rodea.
Esto explica, para Descartes, que la Luna esté en órbita alrededor
de la Tierra y que los objetos que están en la Tierra no se caigan
cuando ésta sigue su órbita alrededor del sol. Esta fuerza de
los torbellinos explica por qué todos los planetas del sistema
solar rotan alrededor del sol en la misma dirección.

El río Lete o Leteo es uno de los ríos del Hades en la mitología
griega. Al beber de sus aguas, los hombres sufrían una profunda
amnesia.

«Cáustico», vale decir, «ardiente, quemante». La alusión a un
espejo cáustico en el relato señala una problemática que todavía
hoy no está esclarecida de manera absoluta en los estudios de
óptica, problemática que tiene su origen en un acontecimiento
supuestamente histórico: en el año 214 a.C., durante las Guerras
Púnicas, el general romano Marcelo sitió Siracusa. Arquímedes,
a quien hoy conocemos más como geómetra, estuvo a cargo de la
defensa de la ciudad como ingeniero militar. Para su defensa,
dicen algunos historiadores, mandó construir unos espejos que
provocaban incendios gracias a la concentración de rayos solares.
Gracias a estos espejos ardientes pudo quemar y destruir las
galeras enemigas.

_+++DA+++_: «Ciencia que trata de la averiguación de las propiedades
y efectos de rayo reflejo». _+++DRAE+++_: «Parte de la óptica
que trata de las propiedades de la luz refleja».

«Multum, crede mihi, refert a fonte bibatur/ quae fluit an pigro
quae stupet unda lacu» (_Epigrammatum_ Lib. IX, C). «¡Oh! créeme:
hay diferencia en beber la cristalina agua corriente, o en beberla
en un charco detenida» (_Epigramas_ Lib. IX, 100).

No se han encontrado referencias a este nombre.

Cita Diógenes Laercio en su _Vida de filósofos_ a un tal Apolodoro,
quien asegura que Pitágoras, al descubrir que, en un triángulo
rectángulo, el cuadrado de la hipotenusa es igual a la suma de
los cuadrados de los dos catetos, realizó una hecatombe. Una
hecatombe, en la Antigua Grecia, era el sacrificio religioso
de cien bueyes, aunque _+++DA+++_ amplía la referencia: «sacrificio
de cien reses de una misma especie, que hacían los griegos y
gentiles cuando se hallaban afligidos de algunas plagas. Por
lo regular era de cien bueyes, cien puercos, ovejas, &c. para
lo cual, según Julio Capitolino, se erigían otros tantos altares
de césped y se ejecutaba a un mismo tiempo por otros tantos sacerdotes.
Es voz griega, que significa cien bueyes».

_+++DA+++_: «Ciencia físico-matemática y la tercera parte de
la óptica, que demuestra las propiedades de los rayos refractos
de la luz, y en consecuencia prescribe reglas y determina las
figuras que deben tener los cristales para que produzcan los
varios efectos que vemos en los anteojos, telescopios, microscopios
y otros instrumentos semejantes». _+++DRAE+++_: «Parte de la
óptica, que trata de los fenómenos de la refracción de la luz».

_+++DA+++_: «Medida que consta de seis pies». _+++DRAE+++_: «Antigua
medida de seis pies».

En la mitología griega, los Campos Elíseos conformaban una parte
del Infierno. En oposición al Tártaro (el lugar del sufrimiento
y el tormento eternos), en estas llanuras sagradas estaban las
sombras de los hombres virtuosos y de los guerreros heroicos,
donde llevaban una existencia apacible en un entorno verde y
florido.

No hemos localizado referencias acerca de este personaje.

La fórmula aparece enmendada en el original.

Apuntado al margen izquierdo.

«Factum est autem ut moreretur mendicus, et portaretur ab angelis
in sinum Abrahae. Mortuus est autem et dives, et sepultus est
in inferno» (Secundum Lucam 16, 22). «Sucedió, pues, que murió
el pobre, y fue llevado por los ángeles al seno de Abraham; y
murió también el rico, y fue sepultado» (Lc 16, 22).

«Tunc dicet et his qui a sinistris erunt: Discedite a me maledicti
in ignem aeternum, qui paratus est diabolo, et angelis eius»
(Secundum Matthaeum 25, 41). «Y dirá a los de la izquierda: Apartaos
de mí, malditos, al fuego eterno, preparado para el diablo y
para sus ángeles» (Mt 25, 41).

«Quoniam non est in morte qui memor sit tui; In inferno autem
quis confitebitur tibi?» (Psalmus 6, 6). «Pues en la muerte no
se hace ya memoria de ti, en el sepulcro, ¿quién te alabará?»
(Sal 6, 6).

«Non mortui laudabunt te, Domine; Neque omnes qui descendunt
in infernum» (Psalmus 113 [hebr. 114-115], 17). «No son los muertos
los que pueden alabar a Yavé, ni cuantos bajaron al silencio»
(Sal 114, 115 [v. 113], 17).

«En el infierno no hay redención» (Responsorio de la lección
VII de _Oficio de Difuntos_).

San Atanasio (296-373) fue obispo de Alejandría y uno de los
principales opositores del arrianismo, doctrina que consideraba
que Jesús no era hijo de Dios o parte de Dios. Hasta el siglo
+++XVII+++ se creyó que san Atanasio había sido el autor del
«quicumque» o símbolo atanasiano, credo que expresa las verdades
esenciales de la fe respecto del misterio de la Santísima Trinidad
y que se recitaba en la liturgia de los domingos. La cita completa
es: «Qui passus est pro salute nostra: descendit ad inferos:
tertia die resurrexit a mortuis» («Que padeció por nuestra salud:
descendió a los infiernos, al tercer día resucitó de entre los
muertos»).

No hemos localizado referencias a este personaje.

En alusión a Tomás de Aquino.

«Sed contra est, quod August. dicit 3, super Gen. ad lit. (cap.
10), _quod aer caliginosus est quasi carcer daemonibus usque
ad tempus judicii_» (_Summa Theologica_, I, q. LXIV, a. IV, s.
c.). «Por otra parte, dice San Agustín que _el aire obscuro es
como una cárcel para los demonios hasta el día del juicio_» (_Suma
Teológica_, I, q. 64, a. 4, s.c.).

Nombrado papa de la Iglesia católica en 1303, muere un año después,
al parecer envenenado.

Al margen izquierdo se lee: «2ª [2?], q 95, a 1 in corpore».

«[…] unde corpora caelestia non possunt esse per se causa operationum
liberi arbitrii» (_Summa Theologica_, II, II, q. 95, a. 5, resp.).
«Los cuerpos celestes, por lo tanto, no pueden producir directamente
los actos propios del libre albedrío» (_Suma Teológica_, II,
II, q. 95, a. 5, resp.).

«Haec dicit Dominus: Iuxta vias gentium nolite discere, Et a
signis caeli nolite me tuere quae timent gentes» (Ieremias, 10,
2). «Así dice Yavé: No os acostumbréis a los caminos de las gentes;
no temáis de los meteoros celestes, que a ellos les producen
terror» (Jer 10, 2).

Prisciliano de Ávila (aprox. 340-385) fue el fundador de una
escuela ascética, el priscilianismo. En su doctrina instaba a
la Iglesia a abandonar la riqueza y opulencia, condenaba la esclavitud
y daba gran importancia a la participación de las mujeres en
la vida religiosa. En 385 Prisciliano es acusado de prácticas
mágicas y oscurantistas por el emperador Máximo en Germania y
es decapitado junto con sus seguidores. Su doctrina continuó
siendo un problema para la Iglesia dos siglos después de su muerte,
y varios concilios se convocaron para estudiar cómo resolverlo,
siendo especialmente importante el Concilio de Braga de 563.

«Si quis animas et corpora humana fatalibus stellis credit adstringi,
sicut pagani et Priscillianus dixerunt, anathema sit» (_Concilium
Bracarense Primun Octoepiscoporum habitum aera +++DXCIX+++, anno
tertio Ariamiri regis, die Kalendarum Maiarum_, 3, IX). «Si alguien
cree que las almas y los cuerpos humanos son constreñidos por
fatales estrellas, según dijeron los paganos y Prisciliano, sea
excomulgado».

Sixto V (1521-1590) fue papa desde 1585 a 1590. Especilamente
recordado por los métodos crueles de su policía vaticana a la
hora de juzgar a los maleantes y prostitutas de Roma.

Al margen derecho se lee: «Autos / [rúbrica]». Al margen izquierdo
se lee: «Presentado en 19 de noviembre de 1776. / Señores Inquisidores
/ Vallejo Galante / Y vistos remítanse [a?] nueva calificación
los párrafos con la nueva exposición [que?] sobre ellos hace
su autor, al Reverendo Padre Diego Marin [rúbrica]. / Se remitieron
en 21 del mismo. / Fray A de [Tomás?] se repitieron a causa de
haber respondido el Padre Marin […] virtud de reconvención […]
por [error?] que no los hallara, ni sabía cómo se habían traspapelado
[rúbrica]».

«Ego dixi in excessu meo: Omnis homo mendax» (Psalmus 115, 115
[hebr. 116], 11). «Habíame dicho en mi abatimiento: “Todos los
hombres son engañosos”» (Sal 116 [v. 114-115], 11).

Tonkín era una región ubicada en lo que hoy es la mayor parte
del norte de Vietnam. Perteneció a China desde el segundo siglo
a. C. hasta la independencia vietnamita en el siglo +++X+++ y
fue protectorado francés desde 1883. Un supuesto incidente bélico
ocurrido en el golfo de Tonkín en 1964 dará lugar a la Guerra
de Vietnam.

El Imperio de Nonomotapa o del Gran Zimbabwe, también llamado
Mwene Mutapa o Manhumutapa, estuvo ubicado en el sur de África
entre 1450 y 1629. Abarcaba los modernos estados de Zimbabwe
y Mozambique.

Gayo Julio Fedro fue un escritor de fábulas de origen macedonio.
Nació en el año 15 a. C. y murió en el 55 d. C. Su colección
de fábulas, muchas inspiradas en Esopo, fueron reunidas en cinco
libros. Era, como también lo fue Esopo, un esclavo, hecho que
vincula el origen de los apólogos con esta condición, ya que,
según el mismo Fedro declara en el prólogo de su obra, al no
poder expresar sus ideas claramente por temor a la reacción violenta
de sus amos, los esclavos las disfrazaron en forma de fábulas.

Flavio Aviano fue un escritor latino de fábulas inspriradas en
Fedro y Babrio. Vivió en el siglo +++IV+++ d. C.

No hay muchos datos ciertos sobre la identidad de Esopo, salvo
que fue un esclavo de la ciudad de Frigia y que murió acusado
de perpetrar un robo en Delfos. Platón menciona que Sócrates
conocía fragmentos de las fábulas de Esopo de memoria.

Andrea Alcíato publicó en 1531 un libro muy popular en su época
y en el siglo siguiente, _Emblematum liber_ o _Libro de los Emblemas_.
Se trata de una colección de 212 poemas emblemáticos latinos
que consisten cada uno en un _motto_ o proverbio, un dibujo y
un texto epigramático.

Salvo el personaje mitológico, no hemos localizado otra referencia
con este nombre.

Menandro nació en Atenas en el 342 a. C. y murió en 292 a. C.
Representante de la llamada «comedia nueva», su teatro se caracteriza
por el tratamiento de temas cotidianos, costumbristas. En oposición
a la «comedia antigua», la del siglo +++IV+++ a. C., en las obras
de Menandro desaparece el coro, los personajes suelen ser tipos
populares y se abandonan los temas heroicos.

Autor de la única novela romana que ha llegado a nuestros días
completa, _El asno de oro_. Nació en Madaura, actual Argelia,
en aprox. 123 d. C. y murió en 180.

Autor de _Los trabajos y los días_, se cree nació cerca de Tebas
en la segunda mitad del siglo +++VIII+++ a. C. o primera mitad
del siguiente. Algunos quieren pensar que fue contemporáneo de
Homero.

Demóstenes nació en Atenas en el 384 a. C. y murió en Calauria
en 322 a. C. Fue uno de los oradores y políticos atenienses más
destacados de la Grecia clásica.

Autor de las _Saturnales_, del _Comentario a los sueños de Escipión
de Cicerón_ y de _Sobre las diferencias y semejanzas del griego
y del latín_, fue un escritor y gramático de Imperio romano del
último cuarto del s. IV d. C.

Autor de las _Noches áticas_, vivió en el siglo +++II+++ de nuestra
era. Se crió en Roma aunque probablemente haya nacido en África.
En su libro anotó desordenadamente curiosidades que oía o leía
en diferentes fuentes, y por ello contiene numerosas anotaciones
sobre historia, gramática, geometría, filosofía, entre otras
materias. Su obra es importante para la historia del pensamiento
porque en ella aparecen fragmentos de otros autores cuyas obras
se han perdido.

Aftonio de Antioquía vivió a finales del siglo +++IV+++. Fue
un reconocido retórico giego y escribió también cuarenta fábulas
esópicas.

Junto con san Atanasio de Alejandría, san Gregorio Nacianceno
y Gregorio de Niza, es uno de los cuatro padres de la Iglesia
griega. Vivió en el siglo +++IV+++ d. C. Se opuso al arrianismo.
Se han conservado muchas de sus homilías, entre ellas una serie
de sermones cuaresmales sobre el _Hexamerón_ o creencia teológica
de que el universo fue creado en seis días. Estos textos aportan
muchas claves acerca del conocimiento científico en el siglo
+++IV+++ d. C.

San Epifanio fue obispo de Salamina y padre de la Iglesia. Vivió
en el siglo +++IV+++ de nuestra era. El _Fisiólogo_ es atribuido
a San Epifanio pero también a muchos otros autores. Escrita en
griego, la obra es una fuente fundamental de los bestiarios medievales,
género de tratados escritos en verso o en prosa que describían
animales reales o imaginarios.

Teodoreto de Ciro nació en Antioquía en 393 y murió en Ciro en
algún momento entre 458 y 466. De formación clásica, fue teólogo
destacado de la escuela de Antioquía y obispo de Ciro en Siria.
Fuerte defensor de Nestorio y crítico de San Cirilo, se retracta
en el Concilio de Calcedonia para poder ser admitido entre los
doctores ortodoxos. _Los diez sermones de la Providencia_ es
una de sus obras apologéticas.

San Gregorio Nacianceno vivió en el siglo +++IV+++ d. C. Fue
obispo de Constantinopla. Es considerado el retórico más completo
de la patrística y su pensamiento tuvo fuerte impacto en la teología
trinitaria.

Sinesio de Cirene fue un filósofo neoplatónico griego que nació
en la actual Libia, ca. 370, y murió en 413 d. C.

Ireneo de Lyon nace en Asia Menor en 130 y muere en Lyon, donde
fue obispo, en 202. Fue discípulo del obispo de Esmirna, Policarpo,
quien a su vez fue discípulo de Juan el apóstol. Intercedió frente
al obispo de Roma en favor de los monatistas, un movimiento herético
de fuerte sentido escatológico y de marcado ascetismo y profetismo.
Polemizó contra los gnósticos en torno al problema de la sustancia
del hombre.

Clemente de Alejandría vivió en el siglo +++II+++ y principios
del III de nuestra era. Acometió uno de los proyectos literarios
más importantes dentro de la historia de la iglesia al escribir
la trilogía _Protrepticus_, _Paedagogus_ y _Stromata_. En esos
tres libros Clemente presenta a los fieles la doctrina cristiana
en la forma tradicional de la literatura secular, y se ocupa
en ellos de denunciar las idolatrías y los misterios paganos,
de detallar el desarrollo de la ética cristiana, y de explicar
cómo se puede perfeccionar la vida cristiana a través del conocimiento.

Cirilo de Alejandría nació en el siglo +++IV+++ d. C. Participó
activamente en la disputa contra Nestorio y su doctrina. Sus
partidarios concurrieron al concilio de Éfeso, en el año 431,
donde discutieron acerca del título que se le debía dar a María:
si Madre de Cristo, como defendían los nestorianos, o Madre de
Dios, como defendían los partidarios de san Cirilo. Se adoptó
como verdad doctrinal la propuesta de Cirilo. En contraparte,
los nestorianos fueron condenados como herejes.

Quinto Séptimo Florente Tertuliano nació, vivió y murió en Cartago
entre los siglos II y III de nuestra era. Hijo de un centurión
romano en África, se dedicó a las leyes hasta que, en 197 o 198,
se convirtió al cristianismo. Luego se separó de la Iglesia católica
para seguir por un tiempo el montanismo y fundar luego su propia
secta opuesta al gnosticismo. Por sus obras doctrinales llegó
a ser maestro de Cipriano de Cartago, predecesor de san Agustín.

Ambrosio de Milán (Tréveris, c. 340 - Milán, 397) fue arzobispo
de Milán. Es uno de los cuatro padres de la Iglesia Latina junto
con san Agustín de Hipona, san Jerónimo de Estridón y san Gregoria
Magno, y uno de los 33 doctores de la Iglesia Católica. Siguió
la guía espiritual de maestros como san Basilio, san Cirilo de
Alejandría y san Gregorio Nacianceno, entre otros.

Se cree nació en Cartagena c. 560, y murió en Sevilla, donde
era arzobispo, en 636. Como teólogo, cronista y compilador, fue
uno de los grandes eruditos de la Edad Media temprana. En _Etimologías_
se ocupa de la cuestión de la historia, a la que define como
un género literario y ubica, en consecuencia, dentro de la gramática.
Sostiene que la historia es la narración de hechos acontecidos
y que etimológicamente designa ver o conocer, aseveración que
lo enfrenta a la posición clásica de Herodoto, para quien la
historia suponía investigar.

Eusebio Hierónimo de Estridón o Jerónimo de Estridón (Dalmacia,
c. 340 - Belén, 420) tradujo la Biblia del griego y el hebreo
al latín. Es uno de los cuatro padres de la Iglesia Latina. Su
traducción de la _Biblia_, conocida como _vulgata_ (de _vulgata
editio_, edición para el pueblo) ha sido el texto bíblico oficial
de la Iglesia Católica hasta la promulgación de la _Neovulgata_
en 1979.

«Quid ergo? praecellimus eos? Nequaquam. Causati enim sumus Iudaeos
et Graecos omnes sub peccato esse, sicut scriptum est: Quia non
est iustus quisquam: Non est intelligens, non est requirens Deum.
Omnes declinaverunt, simul inutiles facti sunt, Non est qui faciat
bonum, non est usque ad unum. Sepulchrum patens est guttur eorum,
Linguis suis dolose agebant: Venenum aspidum sub labiis eorum:
Quorum os maledictione, et amaritudine plenum est: Veloces pedes
eorum ad effundendum sanguinem: Contritio et infelicitas in viis
eorum: Et viam pacis non cognoverunt: Non est timor Dei ante
oculos eorum» (Ad Romanos 3, 9-18). «¿Qué, pues, diremos? ¿Los
aventajamos? No en todo. Pues ya hemos probado que judíos y gentiles
nos hallamos todos bajo el pecado, según que está escrito: “No
hay justo, ni siquiera uno; no hay un sabio, no hay quien busque
a Dios. Todos se han extraviado, todos están corrompidos; no
hay quien haga el bien, no hay ni siquiera uno. Sepulcro abierto
es su garganta, con sus lenguas urden engaños, veneno de áspides
hay bajo sus labios, su boca rebosa maldición y amargura, veloces
son sus pies para derramar sangre, calamidad y miseria abundan
en sus caminos, y la senda de la paz no la conocieron, no hay
temor de Dios ante sus ojos”» (Rm 3, 9-18).

Calímaco nació en Cirene en 310 a. C. y murió en 240 a. C. Fue,
por orden de Ptolomeo II, el encargado de ordenar la biblioteca
de Alejandría, y es por ello considerado el padre de los bibliotecarios.

Epiménides de Cnosos (Creta) vivió en el siglo +++VI+++ a. C.
y fue un poeta, profeta y filósofo griego. Se cuenta que durmió
durante cincuenta y siete años en una cueva bendecida por Zeus
y que despertó dotado de poder visionario.

«Los cretenses son siempre embusteros, malas bestias, glotones
ociosos» (Tito, 1, 12).

«Omnes insulani mali, Siculi autem pessimi». «Todos los isleños
son malvados, pero los sicilianos son peores». La frase es atribuida
a san Pablo cuando pasó por Sicilia hacia Malta.

«Die ac nocte circumdabit eam super muros eius iniquitas; Et
labor in medio eius, Et iniustitia. Et non defecit de plateis
eius usura et dolus» (Psalmus 54 [55], 11-12). «Que día y noche
giran en torno a sus murallas, y en medio de ellas la iniquidad
y la maldad. Dentro de ella está la insidia; de sus plazas no
se apartan nunca la mentira y el fraude» (Sal 55 [v. 54], 11-12).

Al margen derecho se lee: «Autos [rública ilegible]». Al margen
izquierdo se lee: «Presentado en 15 de julio de 1777 / Señores
Inquisidores / Vallejo Galante / Y vistos en catorce de agosto
de mil setecientos setenta y siete por los expresados Señores
dijeron que respecto a no haberse justificado en forma bastante
las producciones delatadas que son del conocimiento privativo
del Santo Oficio, se ponga esta causa en su legajo con la nota
correspondiente en el registro. Y que no teniendo calidad de
oficio el almanaque que por incidente se agregó a ella, como
se ha informado por el Reverendo Padre Diego Marin no tenerla.
Y que por lo que puede hacer contra el honor de los prelados
del denunciado, resulta haber tomado previo conocimiento [el]
Provisor para que pueda continuarle se le devuelven los autos
criminales de este Tribunal [entresacados?] y sellados, por medio
del comisario, expresándole [haber?] levantado la mano el Santo
Oficio tomando el correspondiente recibo. / [Rúbrica ilegible]
/ Con fecha de 19 de agosto se remitieron los autos al Provisor
por medio del comisario.

John Wicklif, o Wickliffe, Wiclef, Wycliff, fue un teólogo y
reformista inglés que vivió durante el siglo +++XIV+++ en Inglaterra.
Es considerado el padre espiritual de los husitas o seguidores
de Juan Hus y, en consecuencia, de los protestantes. Niega la
transustanciación de la eucaristía, que Dios necesite delegados
o intermediarios en curas y sacerdotes, y defiende la abolición
de la propiedad privada incluyendo, por supuesto, los bienes
de la Iglesia. Desafiando la prohibición de la Iglesia, Wicklif
y su grupo de Oxford, donde era profesor de teología, comenzaron
a traducir al inglés la _Vulgata_ en 1378. El papa Gregorio XI
lo acusó en reiteradas ocasiones de herejía. Finalmente, el concilio
de Constanza de 1414 lo declara culpable de herejía y, ya estando
Wicklif muerto, ordena la Iglesia la quema de sus libros, la
exhumación de su cadáver y la quema de sus huesos. El _Dictionnaire
critique, littéraire et bibliographique des principaux Livres
condamnés au feu, supprimés ou censurés_, de Etienne Gabriel
Peignot, publicado por A.A. Renouard en 1806, dice de una de
sus obras: «volume devennu très-rare par la suppression exacte
que la cour de Rome en fit faire».

Nacido en Bohemia del Sur en 1370 y muerto en Constanza en 1415,
Jan Hus, o Juan Huss, o Juan de Hussenitz, fue un teólogo y reformador
checo que predicaba en lengua vernácula, delataba la relajación
moral del clero y defendía el retorno a una iglesia primitiva
sin jerarquías. Nutrido de las ideas de John Wicklif, impulsó
el movimiento cristiano de los husitas. Acudió al Concilio de
Constanza a defender sus ideas y en el mismo concilio que condenó
_post mortem_ a Wicklif, Hus fue también condenado a morir allí
mismo en la hoguera.
