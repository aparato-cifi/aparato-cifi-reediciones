Tomado de la entrada «Ciencia ficción» en Wikipedia (última
consulta, martes 21 de mayo 2019).

_+++DA+++_: «Bailiaje. Especie de encomienda en el orden de caballería
de San Juan, comúnmente hoy llamada de Malta, que obtienen por
su antigüedad los Caballeros profesos y también por gracia particular
del Gran Maestre de la Religión». _+++DRAE+++_ mantiene la definición
del término.

No aparece el término el _+++DA+++_. En _+++DRAE+++_: «Estrecho
de mar».

_+++DA+++_: «El zumo que se saca de las pencas de la hierba llamada
sábila. Viene de la voz árabe cebar, mudada la _e_ en _i_, y añadiéndole
la partícula a se dijo acíbar». _+++DRAE+++_: «áloe, planta.
2. áloe, jugo de esta planta».

_+++DA+++_: «Dicótomo, ma. adj. que sólo tiene uso en la astronomía
para diferenciar la Luna, Venus y Mercurio perfectamente dimidiados
o en la dicotomía de las otras fases o aspectos». En _+++DRAE+++_
el vocablo deja de hacer referencia a la astronomía: «adj. Que
se divide en dos».

_+++DA+++_: «Paralaje o paralaxis. Term. de astrónom. Es la diferencia
del lugar verdadero de un astro, considerando mirarse del centro
de la tierra, al lugar aparente mirado de la superficie de ella».
_+++DRAE+++_: «_Astron_. Diferencia entre las posiciones aparentes
que en la bóveda celeste tiene un astro, según el punto desde
donde se supone observado».

_+++DA+++_: «Equinoccial. adj. de un term. Lo perteneciente al
equinoccio». El término también es aplicado a la línea equinoccial:
«La circunferencia del círculo máximo, que divide el globo terráqueo
en dos partes iguales, que son los hemisferios boreal y austral.
Esta corresponde al ecuador, que se considera en la esfera celeste:
y como en llegando el sol a él se celebran los equinoccios, le
llaman también equinoccial, aunque lo más común es aplicar este
término al de la tierra». _+++DRAE+++_ reitera la primera definición
del término y sólo consigna el uso de «línea equinoccial».

Este es el nombre dado a una supuesta isla que aparece en el
mapa del cosmógrafo Antonio Zeno en el siglo +++XIV+++ y que
estaba ubicada cerca de la Península del Labrador. La isla, no
obstante, nunca fue encontrada, de modo que, desde el siglo +++XVI+++
Estotilandia ha pasado a ser una tierra imaginaria.

El vocablo «turbillón» es una transcripción fónica del término
en francés _tourbillon_, esto es, «torbellino». Descartes utilizó
el término para desarrollar su _théorie des tourbillons_ que explica
la órbita celeste. Para él, el sistema solar es un torbellino
que arrastra los planetas. Cada planeta es el centro de un nuevo
torbellino que retiene en su proximidad la materia que lo rodea.
Esto explica, para Descartes, que la Luna esté en órbita alrededor
de la Tierra y que los objetos que están en la Tierra no se caigan
cuando ésta sigue su órbita alrededor del sol. Esta fuerza de
los torbellinos explica por qué todos los planetas del sistema
solar rotan alrededor del sol en la misma dirección.

El río Lete o Leteo es uno de los ríos del Hades en la mitología
griega. Al beber de sus aguas, los hombres sufrían una profunda
amnesia.

«Cáustico», vale decir, «ardiente, quemante». La alusión a un
espejo cáustico en el relato señala una problemática que todavía
hoy no está esclarecida de manera absoluta en los estudios de
óptica, problemática que tiene su origen en un acontecimiento
supuestamente histórico: en el año 214 a.C., durante las Guerras
Púnicas, el general romano Marcelo sitió Siracusa. Arquímedes,
a quien hoy conocemos más como geómetra, estuvo a cargo de la
defensa de la ciudad como ingeniero militar. Para su defensa,
dicen algunos historiadores, mandó construir unos espejos que
provocaban incendios gracias a la concentración de rayos solares.
Gracias a estos espejos ardientes pudo quemar y destruir las
galeras enemigas.

_+++DA+++_: «Ciencia que trata de la averiguación de las propiedades
y efectos de rayo reflejo». _+++DRAE+++_: «Parte de la óptica
que trata de las propiedades de la luz refleja».

«Multum, crede mihi, refert a fonte bibatur/ quae fluit an pigro
quae stupet unda lacu» (_Epigrammatum_ Lib. IX, C). «¡Oh! créeme:
hay diferencia en beber la cristalina agua corriente, o en beberla
en un charco detenida» (_Epigramas_ Lib. IX, 100).

No se han encontrado referencias a este nombre.

Cita Diógenes Laercio en su _Vida de filósofos_ a un tal Apolodoro,
quien asegura que Pitágoras, al descubrir que, en un triángulo
rectángulo, el cuadrado de la hipotenusa es igual a la suma de
los cuadrados de los dos catetos, realizó una hecatombe. Una
hecatombe, en la Antigua Grecia, era el sacrificio religioso
de cien bueyes, aunque _+++DA+++_ amplía la referencia: «sacrificio
de cien reses de una misma especie, que hacían los griegos y
gentiles cuando se hallaban afligidos de algunas plagas. Por
lo regular era de cien bueyes, cien puercos, ovejas, &c. para
lo cual, según Julio Capitolino, se erigían otros tantos altares
de césped y se ejecutaba a un mismo tiempo por otros tantos sacerdotes.
Es voz griega, que significa cien bueyes».
